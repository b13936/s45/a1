import { Fragment} from 'react'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from '.components/Highlights'
import Home from './pages/Home'

import './App.css';

function App() {
  return (
/*    <div className="App">
    </div>*/
    <Fragment>
      <AppNavbar/>
{/*      <Banner/>
      <Highlights/>*/}
      <Home/>
    </Fragment>
  );
}

export default App;
