import { Row, Col, Card, Button} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row class="course">
			<Col xs={12} md={3}>
				<Card className= "cardHighlight p-3 m-3 mt-3">
					<Card.Body>
						<Card.Title>
							<h2>Sample Courses</h2>
						</Card.Title>
						<Card.Title>
							<h3>Description:</h3>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
						<Card.Title>
							<h3>Price:</h3>
						</Card.Title>
						<Card.Text>
							PHP 40,000
						</Card.Text>
						<Button variant="primary mb-3">
							Enroll Now
						</Button>
					</Card.Body>
				</Card>	
			</Col>	
		</Row>
	)
}