import { Row, Col, Button} from 'react-bootstrap'

export default function Banner() {

	return(
			<Row>
				<Col>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>
						Opportunities for everyone, everywhere
					</p>
					<Button variant="primary mb-3">
						Enroll Now
					</Button>
				</Col>
			</Row>
		)
}