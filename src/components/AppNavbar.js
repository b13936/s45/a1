import { Navbar, Nav} from 'react-bootstrap'


export default function AppNavbar() {
	return	(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>Zuitt</Navbar.Brand>
			<Navbar.Toggle/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>

		</Navbar>
	)
}

